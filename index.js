let theAvengers = ["Iron Man", "Captain America", "Thor", "Hawkeye", "Black Widow"]
console.log(theAvengers);

function newAvenger(addAvenger){
	theAvengers[theAvengers.length ++] = addAvenger;
}

// Add
newAvenger("Hulk")
console.log(theAvengers);

// Pick
function pickItemHero(pickAvenger){
	return theAvengers[pickAvenger];
}
let itemFound = pickItemHero(3);
console.log("Your pick is: " + itemFound)

let theNewAvengers = ["Iron Man", "Captain America", "Thor", "Hawkeye", "Black Widow", "Hulk"]

// Delete

function deleteLastAvenger() {
  let lastAvenger = theNewAvengers[theNewAvengers.length - 1];
  theNewAvengers.pop();
  return lastAvenger;
}

const lastAvenger = deleteLastAvenger();
console.log("The avenger that was removed is:  " + lastAvenger);


// Update

function updatedSuperHero(index, newHero) {
  theNewAvengers[index] = newHero;
}

updatedSuperHero(4, "Captain Marvel");
console.log(theNewAvengers);


// Delete All
function deleteUsers() {
  theNewAvengers = [];
}

deleteUsers();
console.log(theNewAvengers);


// Check if array is empty

function checkEmptyArray() {
  if (theNewAvengers.length > 0) {
    return false;
  } else {
    return true;
  }
}

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);


